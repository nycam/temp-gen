package Templates

import (
	"io"
)

type ITemplate interface {
	//Execute the template process
	//Should ask de user some info to generate the template
	ExecuteTemplate(writer io.Writer)
	//Returns a short description of the template, for CLI usage
	Description() string

	//Returns the type of the template
	//example: Latex
	TemplateType() string
}
