package Templates

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"text/template"
)

type ReactClass struct {
	Name string
}

const ReactClassContent string = `import * as React from 'react'


class {{ .Name }} extends React.Component {
	function render() {
		return (
			<div></div>
		)
	}
}

export default {{ .name }}`

func (this ReactClass) ExecuteTemplate(writer io.Writer) {
	reader := bufio.NewScanner(os.Stdin)
	fmt.Println("Name of the react class: ")
	reader.Scan()
	this.Name = reader.Text()
	temp, err := template.New("React").Parse(ReactClassContent)
	if err != nil {
		panic(err.Error())
	}
	temp.Execute(writer, this)
}

func (this ReactClass) Description() string {
	return "React template class in typescript"
}

func (this ReactClass) TemplateType() string {
	return "React"
}
