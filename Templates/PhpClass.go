package Templates

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"text/template"
)

const PhpClassContent string = `<?php

class {{.ClassName}} {
{{range .Attributs}}
	private ${{.}};
{{end}}
	public function __construct() {

	}
}`

type PhpClass struct {
	ClassName string
	Attributs []string
}

func (this PhpClass) ExecuteTemplate(writer io.Writer) {
	this.Attributs = make([]string, 0)
	reader := bufio.NewScanner(os.Stdin)

	fmt.Println("Name of the class:")
	reader.Scan()
	this.ClassName = reader.Text()

	currentInput := "First"

	for currentInput != "" {

		fmt.Println("Enter an attribute (empty line to stop):")
		reader.Scan()
		currentInput = reader.Text()

		if currentInput != "" {
			this.Attributs = append(this.Attributs, currentInput)
		}
	}

	temp, err := template.New("Php").Parse(PhpClassContent)
	if err != nil {
		panic(err.Error())
	}

	temp.Execute(writer, this)

}

func (this PhpClass) Description() string {
	return "Generates a php class, in which you can specify the name, and a list of private attributes."
}
func (this PhpClass) TemplateType() string {
	return "Php"
}
