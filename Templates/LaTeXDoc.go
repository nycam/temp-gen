package Templates

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"text/template"
)

const docContent string = `\documentclass{{.DocClass}} 

\author{{.DocAuthor}}
\title{{.DocTitle}}
\date{\today}
\begin{document}
	\maketitle
\end{document}
`

type consoleReader struct {
	buffer bufio.Reader
}

type LatexDoc struct {
	DocClass, DocAuthor, DocTitle string
}

func (this LatexDoc) ExecuteTemplate(writer io.Writer) {
	reader := bufio.NewScanner(os.Stdin)

	fmt.Println("Author name:")
	reader.Scan()
	this.DocAuthor = "{" + reader.Text() + "}"

	fmt.Println("document class:")
	reader.Scan()
	this.DocClass = "{" + reader.Text() + "}"

	fmt.Println("document title:")
	reader.Scan()
	this.DocTitle = "{" + reader.Text() + "}"

	template, err := template.New("Latex").Parse(docContent)
	if err != nil {
		panic(err)
	}

	err = template.Execute(writer, this)
	if err != nil {
		panic(err)
	}
}

func (this LatexDoc) TemplateType() string {
	return "LaTeX"
}

func (this LatexDoc) Description() string {
	return "Latex template in which you can specify the class, the title and the author"
}
