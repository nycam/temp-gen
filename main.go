package main

import (
	"flag"
	"fmt"
	"os"
	"temp-gen/Templates"
)

var templates map[string]Templates.ITemplate

func init() {
	templates = make(map[string]Templates.ITemplate)

	latex := Templates.LatexDoc{}
	templates[latex.TemplateType()] = latex

	php := Templates.PhpClass{}
	templates[php.TemplateType()] = php

	reactClass := Templates.ReactClass{}
	templates[reactClass.TemplateType()] = reactClass
}

func main() {
	filePath := flag.String("file", "", "The file to be created")

	fileType := flag.String("type", "", "The type of the file to be created")

	l := flag.Bool("l", false, "List all the templates type")

	desc := flag.String("desc", "", "Show the description for one template")

	flag.Parse()

	if *l {
		listTemplatesType()
	} else if *desc != "" {
		showDescription(*desc)
	} else {
		if template, found := templates[*fileType]; found {
			generateTemplate(*filePath, template)
		} else {
			fmt.Println("Filetype does not exists")
		}
	}

}

func generateTemplate(filePath string, template Templates.ITemplate) {
	file, err := os.Create(filePath)
	if err != nil {
		panic(err.Error())
	}
	defer file.Close()

	template.ExecuteTemplate(file)
}

func listTemplatesType() {
	fmt.Println("There is currently template for:")
	for _, t := range templates {
		fmt.Println("\t-", t.TemplateType())
	}
}

//display description for the type
func showDescription(tempType string) {
	if template, found := templates[tempType]; found {
		fmt.Println("description for: ", tempType)
		fmt.Println(template.Description())
	} else {
		fmt.Println("The type '", tempType, "'does not exists.")
	}
}
